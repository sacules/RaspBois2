
.globl resetLine

resetLine:
	mov x17, x30
	mov x15, 5
Lineloop:
	mov w7, 0xFFFF			// color de la línea
	mov x3, 87				// X de la línea
	mov x4, 0 				// Y de la línea
	mov x5, 336				// ancho de la línea
	mov x6, 10
    bl drawLine
	mov x5, 24

	sub x16, x1, 4			// - if (ya esta abajo del todo) => LretLink
	mov x10, 3
	mul x10, x6, x10
	sub x16 , x16, x10
	cmp x15, x16
    b.eq returnFromLine

    mov x10,0x4EEE			// for i to 61166: nothing
    bl delayLoop

 	add x15, x15, 1

 	b Lineloop

 drawLine:
	mov x18, x30
	add x4, x15, x4			// Agrego el offset

    bl dibujaCuadrado		// dibuja el rectangulo
	sub x4, x4, 1
	bl drawBlack
	add x4, x4, 1

    ret x18


 returnFromLine:
 	ret x17
