.globl elCuadrado


elCuadrado:
	mov x5, 24
	mov x17, x30
	mov x15, 5

cuadradoLoop:
	mov w7, 0xFFE0			// Color, creo que es amarillo
 	mov x3, 87				// X position
	mov x4, 0				// Y position
	bl dibujaSquare
	mov x5, 24

	sub x16, x1, 4
	mov x10, 2
	mul x10, x5, x10
	sub x16 , x16, x10
	cmp x15, x16				// Chequea si pisa el borde de abajo
    b.eq cuadradoReturn

    mov x10,0x3EEE
    bl delayLoop

 	add x15, x15, 1

 	b cuadradoLoop

dibujaSquare:
	mov x18, x30

	mov x10, 2
	mul x5, x5, x10
	mov x6, x5
	add x4, x15, x4			// Agrego el offset

    bl dibujaCuadrado		// dibuja el cuadrado grande

	sub x4, x4, 1			// así no borra el cuadrado antes de dibujarlo
	bl drawBlack
	add x4, x4, 1

    ret x18

 cuadradoReturn:
 	ret x17
